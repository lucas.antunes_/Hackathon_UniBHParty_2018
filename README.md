Por: Kélia Priscila Santos do Amaral | Lucas Antunes | Rai Roney

## _Hackathon UniBHParty 2018_
# Localizador de Recursos Móveis (COP-BH) 


## Informações Gerais

O protótipo foi criado com o objetivo de simular incidentes ocorridos a fim de comparar a agentes municipais, tais como BHTrans, Guarda Municipal, entre outros que estejam nas proximidades e possam ser acionados, agilizando o atendimento e solucionamento de ocorrências.

Para tal, utilizou-se uma API de Transporte Público em Londres para a simulação de agentes e API do Google Maps para a simulação de uma ocorrência e a plotagem do mapa.


As ferramentas e linguagens utilizadas para o desenvolvimento do mesmo contemplam:

- Visual Studio Community 2017
- C# (ASP.NET Web Forms);
- JavaScript
- HTML
 

## Como "rodar" o programa

### Visual Studio Community 2017

> https://www.visualstudio.com/pt-br/thank-you-downloading-visual-studio/?sku=Community&rel=15

No link informado, o download do Visual Studio é iniciado automaticamente. No momento da instalação do software, recomenda-se manter marcadas as opções Desenvolvimento para Desktop com o .NET e ASP.NET e desenvolvimento Web.

Ao finalizar a instalação deve-se realizar os seguintes passos:

- Abrir o Visual Studio e clicar em **File -> Open -> Project/Solution**

![Alt-Text](https://image.prntscr.com/image/WCgF7LYkSSWll1NukkYvpA.png)

- Selecionar o arquivo **Project.sln** localizado na pasta raiz do projeto

![Alt-Text](https://image.prntscr.com/image/00bxZFVmSQqPKPZAZeE11g.png)

- Se necessário, definir o projeto WebApp como padrão de inicialização, clicando com o botão direito do mouse no mesmo -> _**Set as StartUp Project**_

![Alt-Text](https://image.prntscr.com/image/dyqVqW4bSlO4fuYWcfYpFQ.png)

Caso esteja fechada, a aba _**Solution Explorer**_ pode ser aberta através do menu superior, em **View -> Solution Explorer**, ou através da combinação de teclas de atalho **CTRL + ALT + L**

- Iniciar o projeto, clicando no botão abaixo, localizado na parte superior do Visual Studio

![Alt-Text](https://image.prntscr.com/image/pY97Xxi9RUCDqk5xWPJecQ.png)


## Como utilizar

Na tela principal há um simples botão de redirecionamento para uma página onde os usuários poderão inserir dados de localização, isto é, longitude e latitude.

No clique do botão de pesquisa, já na segunda página, há um outro redirecionamento, um mapa é exibidocom a plotagem dos pontos próximos da ocorrência, além de uma janela de informações com diversos dados relevates. 


O uso do sistema é intuitivo, quando o mapa é plotado a ocorrência registrada é centralizada no mapa e seu tooltip, contendo Id, Latitude/Longitude e Data/Hora registro é exibido. Esta tem seu ícone de marcador representando uma batida de carro, a fim de diferenciar dos demais marcadores.

Concomitantemente são exibidos os demais marcadores, que simulam os agentes de campo. Estes também podem ser clicados a qualquer momento, e exibirão os seguintes dados: Id, Latitude/Longitude, Data/Hora em que foram encontrados, Endereço Completo e a Distância em relação à ocorrência registrada.

À direita, na lateral, é exibida uma lista ordenada crescentemente pela distância dos "agentes" em relação à ocorrência (a fim de disponibilizar uma visão mais precisa de quais podem ser acionados), que contem os dados dos marcadores: Distância de referência, Data/Hora em que foram encontrados, e um botão "Ir Para", que trará como foco principal e ao centro o elemento desejado.

**Atualmente, os marcadores exibidos estão limitados a um raio de 3km da ocorrência registrada**.

**Para facilitar a visualização dos resultados segundo o que foi proposto, sugere-se utilizar o conjunto de dados padrão Latitude | Longitude: 51.511412 | -0.123063**, pois o ponto estará mais próximo da requisição realizada em Londres.


### APIs

**OBS:** _As chaves de acesso às APIs já estão inseridas no projeto, mas caso se deseje modificá-las, basta seguir os passos abaixo:_

#### Transport for London Unified API

> https://api.tfl.gov.uk/ 

Será solicitado o registro no site da API para a obtenção de um **App-Id** e uma **App-Key**. O usuário deverá confirmar seu e-mail, e logo após os mesmos estarão disponíveis na aba **API Credentials**.

Após isso, basta substituir as chaves nos devidos campos (**[seu-app-id]**, **[sua-app-key]**), respectivamente, no link abaixo, localizado na variável **urlAPILondres** no caminho do projeto _..\Scripts\MapaDeOcorrencias\geral.js._

##### URL de Requisição
``` 
https://api.tfl.gov.uk/Line/victoria/StopPoints?app_id=[seu-app-id]&app_key=[sua-app-key]
```

#### Google Maps API
> https://developers.google.com/maps/documentation/javascript/?hl=pt-br

Ao acessar o link, basta clicar em obter uma chave, colocar o nome do seu projeto (pode ser qualquer nome), e aceitar os termos de uso da API. 
Então, acesse o arquivo do projeto localizado em _..\MapaDeRecursosMoveis.aspx_ e insira a chave obtida no campo correspondente **[sua-chave]** na tag script, que estará no formato abaixo

##### URL de Requisição
```
https://maps.googleapis.com/maps/api/js?&libraries=geometry&sensor=false&key=[sua-chave]
```