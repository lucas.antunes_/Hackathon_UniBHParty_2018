﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MapaDeRecursosMoveis.aspx.cs" Inherits="WebApp.MapaDeOcorrencias" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../Content/map.css" rel="stylesheet" type="text/css" />
    <link href="../Content/mobile-resources-list.css" rel="stylesheet" type="text/css" />

    <asp:HiddenField runat="server" ID="hdfLatitude" ClientIDMode="Static"/>
    <asp:HiddenField runat="server" ID="hdfLongitude" ClientIDMode="Static"/>

    <div id="exibicaoMapa" class="middle" >
        <hr />
        <br />
        <br />
        <div id="map" hidden="hidden"></div>
        <div id="erroCarregamento" hidden="hidden">
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Erro!</strong> Algum dos campos (Latitude | Longitude) está em branco, faça o relato novamente.
            </div>
            <br />
            <asp:Button runat="server" ID="btnRelatarNovamente" CssClass="custom-btn" Height="50px" Width="250px" Text="Relatar Novamente" OnClick="BtnRelatarNovamenteClick"/>
        </div>
    </div>
    <div id="inner-right">
        <div id="flexlist" class="flexlist flex-container" hidden="hidden">
        </div>
    </div>

    <script src="../Scripts/MapaDeRecursosMoveis/geral.js" type="text/javascript"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?&libraries=geometry&sensor=false&key=AIzaSyBobSBmEY2o9UI8mBeyENPouY5QV5pv0Y4"></script>
</asp:Content>
