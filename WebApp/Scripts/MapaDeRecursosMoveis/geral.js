﻿var lastOpenedInfoWindow;
var arrayMarkers = [];

window.onload = function () {

    var urlAPILondres = "https://api.tfl.gov.uk/Line/victoria/StopPoints?app_id=277e5574&app_key=f251050ce7cb88d9771ef92690608da8";

    requisitaMarcadores(urlAPILondres, function (result) {
        console.log(result);
        montarMapa(result);
        montarFlexboxList(arrayMarkers);

        $('.custom-btn').click(function () {
            if ($(this).attr('data-markerpos')) {
                var posicao = $(this).attr('data-markerpos');
                var map = arrayMarkers[posicao].map;
                map.panTo(arrayMarkers[posicao].getPosition());
                map.setZoom(18);
                closeLastOpenedInfoWindow();
            }
        });

    }, function () {
        var html = $('#exibicaoMapa').empty();
        html.append('<br><div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Ops!</strong> Ocorreu algum problema ao requisitar os dados para o preenchimento no mapa.</div>');
    });
};

function montarMapa(result) {
    var latitude = Number($("#hdfLatitude").val());
    var longitude = Number($("#hdfLongitude").val());
    var dataHoraOcorrencia = localStorage.getItem("dataHoraOcorrencia");
    var dataAtual = moment().format("DD/MM/YYYY HH:mm:ss");

    if (latitude && longitude) {
        $('#map').removeAttr('hidden');
        $('#flexlist').removeAttr('hidden');
        var locale = { lat: 51.511412, lng: -0.123063 };
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: locale
        });
        var ocorrencia = new google.maps.Marker({
            id: 1,
            position: locale,
            lat: locale.lat,
            lng: locale.lng,
            map: map,
            icon: '../Content/Icons/ocorrencia48x48.png',
            title: 'Ocorrência',
            dataRegistro: dataHoraOcorrencia
        });
        var infowindow = new google.maps.InfoWindow({
            content: montarContentOcorrencia(ocorrencia)
        });
        listenerInfoWindow(infowindow, map, ocorrencia);
        infowindow.open(map, ocorrencia);
        lastOpenedInfoWindow = infowindow;
        var coordOcorrencia = new google.maps.LatLng(locale.lat, locale.lng);
        for (var i = 0; i < result.length; i++) {
            var coordRecursoMovel = new google.maps.LatLng(result[i].lat, result[i].lon);
            var distanciaEntrePontos = google.maps.geometry.spherical.computeDistanceBetween(coordOcorrencia, coordRecursoMovel);
            if (distanciaEntrePontos < 3000) {
                var marker = new google.maps.Marker({
                    id: i + 2,
                    position: { lat: result[i].lat, lng: result[i].lon },
                    lat: result[i].lat,
                    lng: result[i].lon,
                    map: map,
                    distanciaOcorrencia: distanciaEntrePontos,
                    dataHoraIdentificacao: dataAtual
                });
                var infoWindowRecursosMoveis = new google.maps.InfoWindow({
                    content: montarContentRecursoMovel(result[i].additionalProperties, marker)
                });
                listenerInfoWindow(infoWindowRecursosMoveis, map, marker);
                arrayMarkers.push(marker);
            }
        }
    }
    else {
        $('#erroCarregamento').removeAttr('hidden');
    }
}

function montarFlexboxList(arrayMarkers) {
    ordenarArrayMarkers(arrayMarkers);

    var html = $('#flexlist').html( );

    html += '<div id="listaRecursosMoveis"><center>';

    for (var i = 0; i < arrayMarkers.length; i++) {
        html += '<div id="recursoMovel' + (i + 1) + '">';
        html += '<label> Data/Hora de identificação: ' + arrayMarkers[i].dataHoraIdentificacao + '</label>';
        html += '<br/>';
        html += '<label> Distância: ' + formatarDistancia(arrayMarkers[i].distanciaOcorrencia) + '</label>';
        html += '<br/>';
        html += '<button type="button" class="custom-btn" style="font-size: 16px;" data-markerpos="' + i + '">Ir para</label>';
        html += '<br/>';
        html += '</div>';
        html += '<hr/>';
        html += '<br/>';
    }

    html += '</center></div>';

    $('#flexlist').html(html);
}

function ordenarArrayMarkers(arrayMarkers) {
    var aux;

    for (var i = 0; i < arrayMarkers.length; i++) {
        for (var j = 1; j < arrayMarkers.length - i; j++) {
            if (arrayMarkers[j - 1].distanciaOcorrencia > arrayMarkers[j].distanciaOcorrencia) {
                aux = arrayMarkers[j - 1];
                arrayMarkers[j - 1] = arrayMarkers[j];
                arrayMarkers[j] = aux;
            }
        }
    }
}

function listenerInfoWindow(infowindow, map, marker) {
    google.maps.event.addListener(marker, 'click', function () {
        map.panTo(marker.getPosition());
        if (map.getZoom < 14) {
            map.setZoom(14);
        }
        closeLastOpenedInfoWindow();
        infowindow.open(map, marker);
        lastOpenedInfoWindow = infowindow;
    });
}

function closeLastOpenedInfoWindow() {
    if (lastOpenedInfoWindow) {
        lastOpenedInfoWindow.close();
    }
}

function montarContentOcorrencia(ocorrencia) {
    var stringContent = "";

    stringContent += 'ID: ' + ocorrencia.id + '<br />';
    stringContent += 'Latitude | Longitude: ' + ocorrencia.lat + " | " + ocorrencia.lng + '<br />';
    stringContent += 'Data/Hora ocorrência: ' + ocorrencia.dataRegistro;
    stringContent += '<br/>';

    return stringContent;
}

function montarContentRecursoMovel(properties, recursoMovel) {
    var stringContent = "";
    
    stringContent += 'ID: ' + recursoMovel.id + '<br />';
    stringContent += 'Latitude | Longitude: ' + recursoMovel.lat + ' | ' + recursoMovel.lng + '<br />';
    stringContent += 'Data/Hora identificação: ' + recursoMovel.dataHoraIdentificacao + '<br />';

    for (var i = 0; i < properties.length; i++) {
        if (properties[i].key === "Address") {
            var address = properties[i].value.split(",");
        }
    }

    if (address) {
        for (var j = 0; j < address.length; j++) {
            stringContent += address[j] + '<br />';
        }
    }

    stringContent += "Distância: " + formatarDistancia(recursoMovel.distanciaOcorrencia);

    return stringContent;
}

function formatarDistancia(distancia) {
    if (distancia >= 1000) {
        distancia /= 1000;
        return parseFloat(distancia).toFixed(1) + " km";
    }
    else {
        return parseFloat(distancia).toFixed(2) + " m";
    }
}

function requisitaMarcadores(url, callback, callbackException) {
    $.ajax({
        type: "GET",
        url: url,
        dataType: "json",
        data: { format: 'json' },
        success: callback,
        error: callbackException
    });
}