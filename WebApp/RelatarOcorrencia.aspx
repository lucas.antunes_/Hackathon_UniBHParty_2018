﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RelatarOcorrencia.aspx.cs" Inherits="WebApp.RelatarOcorrencia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="middle">
        <h2>Relatar Ocorrência</h2>
        <hr />
        <br />
        <div style="margin-left:7px">
            <asp:Label runat="server" Text="Latitude: "></asp:Label>
            <asp:TextBox id="Latitude" class="custom-text-box" runat="server" Height="23px" BorderStyle="Inset"></asp:TextBox>
        </div>
        <br />
        <asp:Label runat="server" Text="Longitude: "></asp:Label>
        <asp:TextBox id="Longitude" class="custom-text-box" runat="server" Height="23px" BorderStyle="Inset"></asp:TextBox>
        <br />
        <br />
        <asp:Button id="btnRelatar" data-id="btnRelatar" class="custom-btn" runat="server" Text="Relatar" OnClick="RelatarBtn"/>
     </div>

    <script src="../Scripts/RelatarOcorrencia/geral.js" type="text/javascript"></script>
</asp:Content>
