﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="WebApp.Main" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <meta charset="utf-8" />
    <div class="middle">
        <h2>Menu principal</h2>
        <hr />
        <br />
        <div>
            <asp:Button CssClass="to-block custom-btn" runat="server" Text="Relatar uma ocorrência" OnClick="BtnRelatarOcorrencia"></asp:Button>
        </div>
    </div>
</asp:Content>
