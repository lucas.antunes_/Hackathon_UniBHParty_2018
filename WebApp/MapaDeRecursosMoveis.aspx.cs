﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class MapaDeOcorrencias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Latitude"] != null && Session["Longitude"] != null)
            {
                hdfLatitude.Value = Session["Latitude"].ToString();
                hdfLongitude.Value = Session["Longitude"].ToString();
            }
        }

        public void BtnRelatarNovamenteClick(object sender, EventArgs e)
        {
            Response.Redirect("RelatarOcorrencia.aspx");
        }
    }
}